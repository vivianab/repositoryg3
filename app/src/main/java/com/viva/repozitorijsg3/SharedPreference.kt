package com.viva.repozitorijsg3

import android.content.Context

class SharedPreference(context: Context){

    val NAME_PREFERENCE = "theSavedName"

    val preference = context.getSharedPreferences(NAME_PREFERENCE, Context.MODE_PRIVATE)

    fun getTheSavedName() : String?{
        return preference.getString(NAME_PREFERENCE, null)
    }


    fun setTheSavedName(name:String){
        val editor = preference.edit()
        editor.putString(NAME_PREFERENCE, name)
        editor.apply()
    }


}
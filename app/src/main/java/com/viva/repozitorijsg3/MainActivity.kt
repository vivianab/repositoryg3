package com.viva.repozitorijsg3

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val usersPreference = SharedPreference(this)


        btnSaveMe.setOnClickListener {
            var theName = inputName.text.toString()
            usersPreference.setTheSavedName(theName)
            Toast.makeText(this, "Saved!", Toast.LENGTH_SHORT).show()
        }

        btnNextActivity.setOnClickListener {
            val intent = Intent(this, SecondActivity::class.java)
            startActivity(intent)
        }

    }

}
